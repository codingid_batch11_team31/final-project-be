CREATE DATABASE db_apple_music;
USE db_apple_music;

-- Tabel untuk kategori produk
CREATE TABLE kategori_produk (
    id INT PRIMARY KEY AUTO_INCREMENT,
    nama_kategori VARCHAR(50) NOT NULL,
    deskripsi VARCHAR(50),
    imageUrl VARCHAR(255) NOT NULL,
    status BIT(1) NOT NULL
);

-- Tabel untuk produk bibit tanaman
CREATE TABLE produk (
    id INT PRIMARY KEY AUTO_INCREMENT,
    nama_produk VARCHAR(50) NOT NULL,
    deskripsi_produk VARCHAR(50),
    harga INT(10) NOT NULL,
    imageUrl VARCHAR(255) NOT NULL,
    status BIT(1) NOT NULL,
    kategori_id INT,
    FOREIGN KEY (kategori_id) REFERENCES kategori_produk(id)
);

-- Tabel untuk metode pembayaran
CREATE TABLE metode_pembayaran (
    id INT PRIMARY KEY AUTO_INCREMENT,
    nama_metode VARCHAR(20) NOT NULL,
    no_rekening VARCHAR(15) NOT NULL,
    imageUrl VARCHAR(255) NOT NULL,
    status BIT(1) NOT NULL
);

-- Tabel untuk pengguna (user)
CREATE TABLE pengguna (
    id INT PRIMARY KEY AUTO_INCREMENT,
    nama VARCHAR(30) NOT NULL,
    email VARCHAR(30) NOT NULL,
    password VARCHAR(30) NOT NULL, 
    role VARCHAR(10) NOT NULL,
    status BIT(1) NOT NULL
);


-- Tabel untuk item keranjang belanja
CREATE TABLE keranjang_item (
    id INT PRIMARY KEY AUTO_INCREMENT,
    schedule DATE NOT NULL, 
    status BIT(1) NOT NULL,
    pengguna_id INT,
    produk_id INT,
    created_at TIMESTAMP,
    FOREIGN KEY (pengguna_id) REFERENCES pengguna(id),
    FOREIGN KEY (produk_id) REFERENCES produk(id)
);

-- Tabel untuk pesanan (pesanan)
CREATE TABLE pesanan (
    id INT PRIMARY KEY AUTO_INCREMENT,
    uuid VARCHAR(35) NOT NULL,
    metode_id INT,
    created_at TIMESTAMP,
    FOREIGN KEY (metode_id) REFERENCES metode_pembayaran(id)
);

-- Tabel untuk detail pesanan
CREATE TABLE detail_pesanan (
    id INT PRIMARY KEY AUTO_INCREMENT,
    pesanan_id INT,
    kItem_id INT,
    FOREIGN KEY (pesanan_id) REFERENCES pesanan(id),
    FOREIGN KEY (kItem_id) REFERENCES keranjang_item(id)
);

ALTER TABLE `kategori_produk` CHANGE `status` `status` BIT(1) NOT NULL DEFAULT b'1';
ALTER TABLE `produk` CHANGE `status` `status` BIT(1) NOT NULL DEFAULT b'1';
ALTER TABLE `pengguna` CHANGE `status` `status` BIT(1) NOT NULL DEFAULT b'1';
ALTER TABLE `metode_pembayaran` CHANGE `status` `status` BIT(1) NOT NULL DEFAULT b'1';
ALTER TABLE `keranjang_item` CHANGE `status` `status` BIT(1) NOT NULL DEFAULT b'1';

ALTER TABLE `pengguna` CHANGE `password` `password` VARCHAR(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL;
